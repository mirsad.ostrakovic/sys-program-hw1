#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

#include <string.h>
#include <stdio.h>

void fatal(char *message);
void fatal2(char *message);

volatile sig_atomic_t SIGINT_count = 0;

volatile sig_atomic_t mojpause_resume = 1;
volatile sig_atomic_t mojpause_signal_trapped;

void init_signal_handlers();


void fatal(char *message)
{
	perror(message);
	exit(-1);
}

void fatal2(char *message)
{
	fprintf(stderr, "%s", message);
	exit(-1);
}



void mojpause_sig_handler(int sig)
{
	sigset_t newmask;
	sigfillset(&newmask);

	mojpause_resume = 0;
	mojpause_signal_trapped = sig;

	sigprocmask(SIG_BLOCK, &newmask, NULL);
}


void mojpause()
{
	struct sigaction sa_array[SIGRTMAX];
	struct sigaction sa;
	sigset_t newmask, oldmask /*, zeromask*/;

	sigfillset(&newmask); 
	//sigemptyset(&zeromask);

	sa.sa_handler = mojpause_sig_handler;
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART;

	// start critical section
	sigprocmask(SIG_BLOCK, &newmask, &oldmask);

	for(int i = 0; i < SIGRTMAX; ++i)
		sigaction(i+1, &sa, &sa_array[i]);
	
	mojpause_resume = 1;
	//sigfillset(&newmask); 
	
	sigprocmask(SIG_SETMASK, &oldmask, NULL);
	// end critical section

	while(mojpause_resume);

	// if any signal occur here it will overide old signal :(

	// here start critical section
	//sigprocmask(SIG_BLOCK, &newmask, &oldmask);
	
	if((sa_array[mojpause_signal_trapped-1].sa_flags & SA_SIGINFO) == SA_SIGINFO)
		sa_array[mojpause_signal_trapped-1].sa_sigaction(mojpause_signal_trapped, NULL, NULL);
	else
		sa_array[mojpause_signal_trapped-1].sa_handler(mojpause_signal_trapped);


	for(int i = 0; i < SIGRTMAX; ++i)
		sigaction(i+1, &sa_array[i], NULL);
	

	sigprocmask(SIG_SETMASK, &oldmask, NULL);
	// end crictial section

}


int main(void)
{

	init_signal_handlers();
	alarm(3);

	while(1)
		mojpause();

	return 0;
}




void SIGQUIT_handler(int sig)
{
	char buff[32];
	int n;

	if((n = sprintf(buff, "SIGINT_count: %d\r\n", SIGINT_count)) < 0)
		fatal2("SIGQUIT_handler(): sprintf failed");

	if((n = write(STDOUT_FILENO, buff, n)) == -1)
		fatal("SIGQUIT_handler(): write failed");

	exit(0);
}


void SIGINT_handler(int sig)
{
	++SIGINT_count;
}

void SIGALRM_handler(int sig)
{
	char buff[32];
	int n;	

	//need to check sprintf return value
	if((n = sprintf(buff, "Pokrenut sam...\r\n")) < 0)
		fatal2("SIGALRM_handler(): sprintf failed");

	if((n = write(STDOUT_FILENO, buff, strlen(buff))) == -1)
		fatal("SIGALRM_handler(): write failed");

	alarm(3);
}


void init_signal_handlers()
{
	struct sigaction sa;

	// SIGQUIT - terminate program and write SIGINT_count
	sa.sa_handler = SIGQUIT_handler;
	sigemptyset(&sa.sa_mask); // block all signals while executing the SIGQUIT handler
	sa.sa_flags = SA_RESTART;
	sigaction(SIGQUIT, &sa, NULL);

	// SIGINT - count occurence od SIGINT
	sa.sa_handler = SIGINT_handler;
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART;
	sigaction(SIGINT, &sa, NULL);

	// SIGALRM - write to STDOUT_FILENO "Pokrenut sam..." every 3 seconds
	sa.sa_handler = SIGALRM_handler;
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART;
	sigaction(SIGALRM, &sa, NULL);

}
