#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <sys/signalfd.h>

#include <string.h>
#include <stdio.h>

void fatal(char *message);
void fatal2(char *message);
void init_signal_handlers();

volatile sig_atomic_t SIGINT_count = 0;


void mojpause()
{
	struct signalfd_siginfo siginfo;
	struct sigaction sigact;
	sigset_t sigset;
	int sig_fd, n;

	if(sigfillset(&sigset) == -1)
		fatal("mojpause(): sigfillset(&sigset) fail");

	if((sig_fd = signalfd(-1, &sigset, 0)) == -1)
		fatal("mojpause(): signalfd(-1, &sigset, 0) fail");

	if((n = read(sig_fd, &siginfo, sizeof(siginfo))) == -1)
		fatal("mojpause(): read(sig_fd, &siginfo, sizeof(siginfo)) fail");
	
	if(close(sig_fd) == -1)
		fatal("mojpause(): close(sig_fd) fail");
	
	if(n != sizeof(siginfo))
		fatal2("mojpause(): read bytes != sizeof(siginfo)");

	if(sigaction(siginfo.ssi_signo, NULL, &sigact) == -1)
		fatal("mojpause(): sigaction() fail");

	if((sigact.sa_flags & SA_SIGINFO) == SA_SIGINFO)
		sigact.sa_sigaction(siginfo.ssi_signo, NULL, NULL);
	else
		sigact.sa_handler(siginfo.ssi_signo);

/*
	char buff[128];
	sprintf(buff, "mojpause() code: %d signo: %d\r\n", siginfo.ssi_code, siginfo.ssi_signo);
	write(STDOUT_FILENO, buff, strlen(buff));
*/
}


int main(void)
{

	init_signal_handlers();
	alarm(3);

	while(1)
		mojpause();
		//pause();

	return 0;
}



void fatal(char *message)
{
	perror(message);
	exit(-1);
}

void fatal2(char *message)
{
	fprintf(stderr, "%s", message);
	exit(-1);
}


void SIGQUIT_handler(int sig)
{
	char buff[32];
	int n;

	if((n = sprintf(buff, "SIGINT_count: %d\r\n", SIGINT_count)) < 0)
		fatal2("SIGQUIT_handler(): sprintf failed");

	if((n = write(STDOUT_FILENO, buff, n)) == -1)
		fatal("SIGQUIT_handler(): write failed");

	exit(0);
}


void SIGINT_handler(int sig)
{
	++SIGINT_count;
}

void SIGALRM_handler(int sig)
{
	char buff[32];
	int n;	

	//need to check sprintf return value
	if((n = sprintf(buff, "Pokrenut sam...\r\n")) < 0)
		fatal2("SIGALRM_handler(): sprintf failed");

	if((n = write(STDOUT_FILENO, buff, strlen(buff))) == -1)
		fatal("SIGALRM_handler(): write failed");

	alarm(3);
}


void init_signal_handlers()
{
	struct sigaction sa;

	// SIGQUIT - terminate program and write SIGINT_count
	sa.sa_handler = SIGQUIT_handler;
	sigemptyset(&sa.sa_mask); // block all signals while executing the SIGQUIT handler
	sa.sa_flags = SA_RESTART;
	sigaction(SIGQUIT, &sa, NULL);

	// SIGINT - count occurence od SIGINT
	sa.sa_handler = SIGINT_handler;
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART;
	sigaction(SIGINT, &sa, NULL);

	// SIGALRM - write to STDOUT_FILENO "Pokrenut sam..." every 3 seconds
	sa.sa_handler = SIGALRM_handler;
	sigemptyset(&sa.sa_mask); 
	sa.sa_flags = SA_RESTART;
	sigaction(SIGALRM, &sa, NULL);

}
