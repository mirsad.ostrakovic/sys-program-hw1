#ifndef	FILESTAT_H
#define FILESTAT_H

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <string>
#include <stdexcept>

class FileStat{
	public:
		FileStat(const std::string& filenameParam,
						 ino_t inodeParam,
						 mode_t modeParam,
						 nlink_t nlinkParam,
						 uid_t uidParam,
						 gid_t gidParam,
						 off_t sizeParam,
						 time_t modifTimeParam);


	
		bool operator<(const FileStat&);

		void setUsername(const std::string& u);
		void setGroupname(const std::string& g); 
		const std::string& getUsername() const { return username; }
		const std::string& getGroupname() const { return groupname; }

		const std::string filename; // filename	
		const ino_t inode; // inode number
		const mode_t mode; // file type ande mode
		const nlink_t nlink; // number of hard links
		const uid_t uid; // user ID of owner
		const gid_t gid; // group ID of owner
		const off_t size; // total size, in bytes
		const time_t modifTime; // time of last modification
		const std::string modeFormated;
		const std::string modifTimeFormated;


		static void remapStat();
		static bool isStatRemapped() { return isRemapped; }
		static std::size_t getFilenameMaxLen() { return filenameMaxLen; }
		static std::size_t getInodeMaxLen() { return inodeMaxVal; }	
		static std::size_t getNlinkMaxLen() { return nlinkMaxVal; }
		static std::size_t getUidMaxLen() { return uidMaxVal; }
		static std::size_t getGidMaxLen() { return gidMaxVal; }
		static std::size_t getSizeMaxLen() { return sizeMaxVal; }
		static std::size_t getUsernameMaxLen() { return usernameMaxLen; }
		static std::size_t getGroupnameMaxLen() { return groupnameMaxLen; }

		static void reinitStat();


	private:
		static std::string initModeFormated(ino_t mode);	
		static std::string timeFormated(time_t);
		static int digitCount(unsigned long val);
		
		static bool isRemapped;
		static std::size_t filenameMaxLen;
		static ino_t inodeMaxVal; 
		static nlink_t nlinkMaxVal; 
		static uid_t uidMaxVal; 
		static gid_t gidMaxVal; 
		static off_t sizeMaxVal; 

		std::string username;
		std::string groupname;
		static std::size_t usernameMaxLen;
		static std::size_t groupnameMaxLen;
};


bool operator<(const FileStat& fs1, const FileStat& fs2);

void print(const FileStat& fileStat);

#endif
