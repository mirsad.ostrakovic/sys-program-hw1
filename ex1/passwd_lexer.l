%option prefix="passwd_"

%{
#include <string>
#include "passwd_parser.tab.h"

%}


%%

[^:\n]+				{ passwd_lval.strPtr = new std::string(yytext); return PASSWD_TOKEN; }
[:]						{ return PASSWD_DELIM; }
[\n]					{ return PASSWD_NEWLINE; }


%%

int yywrap(){
	return 1;
}

