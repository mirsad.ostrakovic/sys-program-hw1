/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_PASSWD_PARSER_TAB_H_INCLUDED
# define YY_PASSWD_PARSER_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef PASSWD_DEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define PASSWD_DEBUG 1
#  else
#   define PASSWD_DEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define PASSWD_DEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined PASSWD_DEBUG */
#if PASSWD_DEBUG
extern int passwd_debug;
#endif

/* Token type.  */
#ifndef PASSWD_TOKENTYPE
# define PASSWD_TOKENTYPE
  enum passwd_tokentype
  {
    TOKEN = 258,
    DELIM = 259,
    NEWLINE = 260
  };
#endif

/* Value type.  */
#if ! defined PASSWD_STYPE && ! defined PASSWD_STYPE_IS_DECLARED

union PASSWD_STYPE
{
#line 20 "parser.y" /* yacc.c:1909  */

	std::string *strPtr;

#line 72 "parser.tab.h" /* yacc.c:1909  */
};

typedef union PASSWD_STYPE PASSWD_STYPE;
# define PASSWD_STYPE_IS_TRIVIAL 1
# define PASSWD_STYPE_IS_DECLARED 1
#endif


extern PASSWD_STYPE passwd_lval;

int passwd_parse (void);

#endif /* !YY_PASSWD_PARSER_TAB_H_INCLUDED  */
