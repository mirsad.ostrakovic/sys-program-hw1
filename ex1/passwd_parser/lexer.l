%option prefix="passwd_"

%{
#include <string>
#include "passwd_.tab.h"

%}


%%

[^:\n]+				{ passwd_lval.strPtr = new std::string(yytext); return TOKEN; }
[:]						{ return DELIM; }
[\n]					{ return NEWLINE; }


%%

int yywrap(){
	return 1;
}

