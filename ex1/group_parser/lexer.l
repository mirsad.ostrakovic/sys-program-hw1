%option prefix="group_"
%option noyywrap

%{
#include <string>
#include "parser.tab.h"

%}


%%

[^:\n]+				{ group_lval.strPtr = new std::string(yytext); return TOKEN; }
[:]						{ return DELIM; }
[\n]					{ return NEWLINE; }


%%

