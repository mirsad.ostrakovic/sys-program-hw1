%define parse.error verbose
%define api.prefix {group_}

%{
#include<iostream>
#include<string>
#include<map>

std::map<uid_t,std::string> gidToUsernameMap;

int yylex();
void yyerror(const char *error);


%}

%token<strPtr> TOKEN 
%token DELIM NEWLINE

%union{
	std::string *strPtr;
};

%type<strPtr> item

%%


file_lines:			file_line NEWLINE
							|	file_line NEWLINE file_lines
;

file_line: 			item DELIM 
								item DELIM
								item DELIM
								item 					{
																if($1 == nullptr || $5 == nullptr)
																{
																	std::cerr << "Invalid file format" << std::endl;
																	exit(-1);
																}
													
																gidToUsernameMap.insert({std::stoul(*$5), *$1});
																delete $1;
																delete $3;
																delete $5;
																delete $7;
															}
;

item:						%empty				{ $$ = nullptr; } 
						 |  TOKEN					{ $$ = $1; }
;


%%

void yyerror(const char *error){
	std::cerr << "yyerror(): " << error << std::endl;
}

/*
int main()
{
	group_parse();
	
	for(auto it = std::begin(gidToUsernameMap); it != std::end(gidToUsernameMap); ++it)
		std::cout << it->first << " " << it->second << std::endl;
}
*/
