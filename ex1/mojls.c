#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>



int main(int argc, const char *argv[])
{

	if(argc < 2)
	{
		fprintf(stderr, "Invalid number of args\r\n");	
	}

	DIR *currentDirectory;
	struct dirent* currentDirectoryEntry;
	int fd;
	struct stat fileStat;

	errno = 0;

	if((currentDirectory = opendir(argv[1])) == NULL)
	{
		perror("main()");	
		exit(-1);	
	}



	errno = 0;

	while((currentDirectoryEntry = readdir(currentDirectory)) != NULL)
	{
	
		printf("-------------------------------------------\r\n");
		printf("Inode number %lu\r\n", currentDirectoryEntry->d_ino);
		printf("d_off: %lu\r\n", currentDirectoryEntry->d_off);
		printf("Size of returned record: %d\r\n", currentDirectoryEntry->d_reclen);
		printf("File type: %d\r\n", currentDirectoryEntry->d_type);
		
		switch(currentDirectoryEntry->d_type)
		{
			case DT_BLK:
				printf("BLOCK DEVICE\r\n");
				break;

			case DT_CHR:
				printf("CHARACTER DEVICE\r\n");
				break;

			case DT_DIR:
				printf("DIRECTORY\r\n");
				break;

			case DT_FIFO:
				printf("NAMED FIFO\r\n");
				break;

			case DT_LNK:
				printf("SYMBOLIC LINK\r\n");
				break;

			case DT_REG:
				printf("REGULAR FILE\r\n");
				break;

			case DT_SOCK:
				printf("UNIX DOMAIN SOCKET\r\n");
				break;

			case DT_UNKNOWN:
				printf("UNKOWN FILE TYPE\r\n");
				break;
		}

		printf("Filename: %s\r\n", currentDirectoryEntry->d_name);


		if((fd = open(currentDirectoryEntry->d_name, O_RDONLY)) == -1)
		{
			perror("main()");
			exit(-1);
		}

		if(fstat(fd, &fileStat) == -1)
		{
			perror("main()");
			exit(-1);
		}


		printf("st_dev: %lu\r\n", fileStat.st_dev);
		printf("st_ino: %lu\r\n", fileStat.st_ino);
		printf("st_mode: %u\r\n", fileStat.st_mode);
		printf("st_nlink: %lu\r\n", fileStat.st_nlink);
		printf("st_uid: %u\r\n", fileStat.st_uid);
		printf("st_gid: %u\r\n", fileStat.st_gid);
		printf("st_rdev: %lu\r\n", fileStat.st_rdev);
		printf("st_size: %lu\r\n", fileStat.st_size);
		printf("st_blksize: %lu\r\n", fileStat.st_blksize);
		printf("st_blocks: %lu\r\n", fileStat.st_blocks);
		printf("st_atim: %lu\r\n", fileStat.st_atime);

		printf("-------------------------------------------------------\r\n\r\n");
	}

	if(errno != 0)
	{
		perror("main()");
		exit(-1);
	}

	if(closedir(currentDirectory) == -1)
	{
		perror("main()");
		exit(-1);
	}



	return 0;
}
