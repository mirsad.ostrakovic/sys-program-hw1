#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <ctime>
#include <cstring>
#include <map>
#include <set>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "passwd_parser.tab.h"
#include "group_parser.tab.h"

#include "filestat.h"

extern std::map<uid_t,std::string> uidToUsernameMap;
extern std::map<uid_t,std::string> gidToGroupnameMap;


void fatal(const char *msg)
{
		perror(msg);
		exit(-1);
}


void fatal2(const char *message)
{
	fprintf(stderr, "%s", message);
	exit(-1);
}




void list_directory(const char* pathname)
{
	DIR *curDir;
	struct dirent* curDirEnt;
	int fd;
	struct stat fileStat;
	std::set<FileStat> fileStatSet;

	errno = 0;

	if((curDir = opendir(pathname)) == NULL)
		fatal("list_directory(): can not open directory");	
	

	errno = 0;

	while((curDirEnt = readdir(curDir)) != NULL)
	{
		std::string directoryName = std::string(pathname) + '/' + curDirEnt->d_name;
		
		if((fd = open(directoryName.c_str(), O_RDONLY)) == -1)
			fatal("Can not open file");
		
		if(fstat(fd, &fileStat) == -1)
			fatal("Can not get fstat");

		FileStat fs(curDirEnt->d_name,
								fileStat.st_ino,
							  fileStat.st_mode,
								fileStat.st_nlink,
								fileStat.st_uid,
								fileStat.st_gid,
								fileStat.st_size,
								fileStat.st_mtime);

		fileStatSet.insert(std::move(fs));
	}

	if(errno != 0)
		fatal("Error in readdir()");

	if(closedir(curDir) == -1)
		fatal("Error in closing curDir");


	
	FileStat::remapStat();
	
	for(auto it = std::begin(fileStatSet); it != std::end(fileStatSet); ++it)
	{
		FileStat &fs = const_cast<FileStat&>(*it);

		{
			auto it = uidToUsernameMap.find(fs.uid);
			if(it != std::end(uidToUsernameMap))
				fs.setUsername(it->second);
		}

		{
			auto it = gidToGroupnameMap.find(fs.gid);
			if(it != std::end(gidToGroupnameMap))
				fs.setGroupname(it->second);
		}
	}


	for(auto it = std::begin(fileStatSet); it != std::end(fileStatSet); ++it)
	{
		print(*it);
		printf("\n");
	}

}





int main(int argc, const char *argv[])
{

	int terminalFd, passwdFd, groupFd;

	if((terminalFd = dup(STDIN_FILENO)) == -1)
		fatal("Can not duplicate STDIN file descriptior");

	if((passwdFd = open("/etc/passwd", O_RDONLY)) == -1)
		fatal("Can not open /etc/passwd");
	
	if((groupFd = open("/etc/group", O_RDONLY)) == -1)
		fatal("Can not open /et/group");


	if(dup2(passwdFd, STDIN_FILENO) == -1)
		fatal("Can not dup passwFd file as STDIN");

	if(close(passwdFd) == -1)
		fatal("Can not close passwdFd");

	passwd_parse();

	if(dup2(groupFd, STDIN_FILENO) == -1)
		fatal("Can not dup groupFd file as STDIN");	

	if(close(groupFd) == -1)
		fatal("Can not close groupFd");

	group_parse();


	if(dup2(terminalFd, STDIN_FILENO) == -1)
		fatal("Can not dup terminalFd file as STDIN");	

	if(close(terminalFd) == -1)
		fatal("Can not close terminalFd");


	std::vector<const char *> listItems = {&argv[1], &argv[1] + argc - 1};

	if(listItems.size() == 0u)
		listItems.push_back(".");


	for(auto it = std::begin(listItems); it != std::end(listItems); ++it)
	{
		if(printf("%s:\n", *it) < 0)
			fatal2("printf() foldername failed");

		list_directory(*it);
		FileStat::reinitStat();
	}

	return 0;
}
