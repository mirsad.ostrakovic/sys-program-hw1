#include "filestat.h"

void fatal(const char *msg);
void fatal2(const char *msg);

bool FileStat::isRemapped = false;
std::size_t FileStat::filenameMaxLen = 0u;
ino_t FileStat::inodeMaxVal = 0; 
nlink_t FileStat::nlinkMaxVal = 0; 
uid_t FileStat::uidMaxVal = 0; 
gid_t FileStat::gidMaxVal = 0; 
off_t FileStat::sizeMaxVal = 0; 


std::size_t FileStat::usernameMaxLen = 0u;
std::size_t FileStat::groupnameMaxLen = 0u;



void FileStat::remapStat()
{
	if(isRemapped == false)
	{
		inodeMaxVal = digitCount(inodeMaxVal);
		nlinkMaxVal = digitCount(nlinkMaxVal);
		uidMaxVal = digitCount(uidMaxVal);
		gidMaxVal = digitCount(gidMaxVal);
		sizeMaxVal = digitCount(sizeMaxVal);
		isRemapped = true;
	}
}



std::string FileStat::initModeFormated(ino_t mode)
{

	char mod[10];

	mod[0] = ((mode & S_IFMT) == S_IFDIR) ? 'd' : '-';
	mod[1] = ((mode & S_IRUSR) == S_IRUSR ? 'r' : '-');
	mod[2] = ((mode & S_IWUSR) == S_IWUSR ? 'w' : '-');
	mod[3] = ((mode & S_IXUSR) == S_IXUSR ? 'x' : '-');
	mod[4] = ((mode & S_IRGRP) == S_IRGRP ? 'r' : '-');
	mod[5] = ((mode & S_IWGRP) == S_IWGRP ? 'w' : '-');
	mod[6] = ((mode & S_IXGRP) == S_IXGRP ? 'x' : '-');
	mod[7] = ((mode & S_IROTH) == S_IROTH ? 'r' : '-');
	mod[8] = ((mode & S_IWOTH) == S_IWOTH ? 'w' : '-');
	mod[9] = ((mode & S_IXOTH) == S_IXOTH ? 'x' : '-');

	return std::string(mod);
}




FileStat::FileStat(const std::string& filenameParam,
									 ino_t inodeParam,
						 			 mode_t modeParam,
						 			 nlink_t nlinkParam,
						 			 uid_t uidParam,
						 			 gid_t gidParam,
						 			 off_t sizeParam,
						 			 time_t modifTimeParam):
	  filename{filenameParam},	
		inode{inodeParam},
		mode{modeParam},
		nlink{nlinkParam},
		uid{uidParam},
		gid{gidParam},
		size{sizeParam},
		modifTime{modifTimeParam},
		modeFormated{std::move(initModeFormated(modeParam))},
		modifTimeFormated{std::move(timeFormated(modifTimeParam))}
{
	if(isRemapped == true) throw std::domain_error("isRemapped() is already called, so you can not make new instance of this object");
	if(filename.size() > filenameMaxLen) filenameMaxLen = filename.size();
	if(inode > inodeMaxVal) inodeMaxVal = inode;
	if(nlink > nlinkMaxVal) nlinkMaxVal = nlink;
	if(uid > uidMaxVal) uidMaxVal = uid;
	if(gid > gidMaxVal) gidMaxVal = gid;
	if(size > sizeMaxVal) sizeMaxVal = size;
}



void FileStat::setUsername(const std::string& u) 
{
	username = u;

	if(username.size() > usernameMaxLen)
		usernameMaxLen = username.size();
}



void FileStat::setGroupname(const std::string& g)
{
	groupname = g;

	if(groupname.size() > groupnameMaxLen)
		groupnameMaxLen = groupname.size();
}





int FileStat::digitCount(unsigned long val)
{
	int digitCount = 0u;

	do{
		++digitCount;
		val /= 10;
	}
	while(val);

	return digitCount;
}






std::string FileStat::timeFormated(time_t tim)
{
	char buff[64];
	struct tm *time = localtime(&tim);

	sprintf(buff, "%4d-%02d-%02d %02d:%02d", 1900+time->tm_year, time->tm_mon+1, time->tm_mday, time->tm_hour, time->tm_min);
	return buff;
}


void FileStat::reinitStat()
{
		isRemapped = false;
		filenameMaxLen = 0u;
		inodeMaxVal = 0u; 
		nlinkMaxVal = 0u; 
		uidMaxVal = 0u; 
		gidMaxVal = 0u; 
		sizeMaxVal = 0u; 
		usernameMaxLen = 0u;
		groupnameMaxLen = 0u;
}



bool FileStat::operator<(const FileStat& fs)
{
	return filename < fs.filename;
}




bool operator<(const FileStat& fs1, const FileStat& fs2)
{
	return fs1.filename < fs2.filename;
}



void print(const FileStat& fileStat)
{
	
	if(printf("%*lu %s %*lu ", 
				FileStat::getInodeMaxLen(), fileStat.inode,
				fileStat.modeFormated.c_str(),
				FileStat::getNlinkMaxLen(), fileStat.nlink) < 0)	
			fatal2("print(): printf() failed");

	if(fileStat.getUsername().size() == 0u)
	{
		if(printf("%*u ", FileStat::getUidMaxLen(), fileStat.uid) < 0)
			fatal2("print(): printf() failed");
	}	
	else
	{
		if(printf("%*s ", FileStat::getUsernameMaxLen(), fileStat.getUsername().c_str()) < 0)
			fatal2("print(): printf() failed");
	}

	if(fileStat.getGroupname().size() == 0u)
	{
		if(printf("%*u ", FileStat::getGidMaxLen(), fileStat.gid) < 0)
			fatal2("print(): printf() failed");
	}
	else
	{
		if(printf("%*s ", FileStat::getGroupnameMaxLen(), fileStat.getGroupname().c_str()) < 0)
			fatal2("print(): printf() failed");
	}
 	
	
	if(printf("%*lu %s ",	
				FileStat::getSizeMaxLen(), fileStat.size,
				fileStat.modifTimeFormated.c_str()) < 0)
			fatal2("print(): printf() failed");

	switch(fileStat.mode & S_IFMT)
	{
		case S_IFDIR:
			if(printf("\033[1;34m%s\033[0m/", fileStat.filename.c_str()) < 0)
				fatal2("print(): printf() failed");
			break;

		case S_IFREG:
			if(fileStat.mode & (S_IXUSR | S_IXGRP | S_IXOTH))
			{	if(printf("\033[1;32m%s\033[0m*", fileStat.filename.c_str()) < 0)
					fatal2("print(): printf() failed");
			}
			else
			{
				if(printf("%s", fileStat.filename.c_str()) < 0)
					fatal2("print(): printf() failed");
			}
			break;

		default:
			if(printf("%s", fileStat.filename.c_str()) < 0)
				fatal2("print(): printf() failed");
	}
}


