#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>

#include <ctime>
#include <cstring>
#include <map>
#include <string>
#include <iostream>
#include <algorithm>

#include "passwd_parser.tab.h"
#include "group_parser.tab.h"

class FileStat;


struct FileStat{

	FileStat(ino_t inode_p,
					 mode_t mode_p,
					 nlink_t nlink_p,
					 uid_t uid_p,
					 gid_t gid_p,
					 off_t size_p,
					 time_t mtime_p):
		inode{inode_p},
		mode{mode_p},
		nlink{nlink_p},
		uid{uid_p},
		gid{gid_p},
		size{size_p},
		mtime{mtime_p}
	{}

	std::string getMode() const;


	ino_t inode; // inode number
	mode_t mode; // file type ande mode
	nlink_t nlink; // number of hard links
	uid_t uid; // user ID of owner
	gid_t gid; // group ID of owner
	off_t size; // total size, in bytes
	time_t mtime; // time of last modification
};

std::string FileStat::getMode() const 
{  
	char mod[10];
	mod[0] = ((mode & S_IFMT) == S_IFDIR) ? 'd' : '-';
	mod[1] = ((mode & S_IRUSR) == S_IRUSR ? 'r' : '-');
	mod[2] = ((mode & S_IWUSR) == S_IWUSR ? 'w' : '-');
	mod[3]	= ((mode & S_IXUSR) == S_IXUSR ? 'x' : '-');
	mod[4] = ((mode & S_IRGRP) == S_IRGRP ? 'r' : '-');
	mod[5] = ((mode & S_IWGRP) == S_IWGRP ? 'w' : '-');
	mod[6] = ((mode & S_IXGRP) == S_IXGRP ? 'x' : '-');
	mod[7] = ((mode & S_IROTH) == S_IROTH ? 'r' : '-');
	mod[8] = ((mode & S_IWOTH) == S_IWOTH ? 'w' : '-');
	mod[9] = ((mode & S_IXOTH) == S_IXOTH ? 'x' : '-');

	return mod;
}


struct FormatStat{

	FormatStat():
		max_pathname_len{0},
		max_inode{0},
		max_nlink{0},
		max_uid{0},
		max_gid{0},
		max_size{0}
	{}



	FormatStat(std::size_t pathname_len, const FileStat& fs):
		max_pathname_len{pathname_len},
		max_inode{fs.inode},
		max_nlink{fs.nlink},
		max_uid{fs.uid},
		max_gid{fs.gid},
		max_size{fs.size}
	{}



	void update(std::size_t pathname_len, const FileStat& fs);

	std::size_t max_pathname_len;
	ino_t max_inode; // inode number
	//mode_t mode; // file type ande mode
	nlink_t max_nlink; // number of hard links
	uid_t max_uid; // user ID of owner
	gid_t max_gid; // group ID of owner
	off_t max_size; // total size, in bytes
	//time_t mtime; // time of last modification
};

int digitCount(unsigned long val)
{
	int retVal = 0u;

	if(val == 0u)
		return 1u;

	while(val)
	{
		val /= 10;
		++retVal;
	}

	return retVal;
}

void print_time(const time_t timer)
{
	struct tm *time = gmtime(&timer);

	// year month date hours minutes
	printf("%4d-%02d-%02d %02d:%02d", 1900+time->tm_year, time->tm_mon+1, time->tm_mday, time->tm_hour, time->tm_min);
}


void print(const FileStat& fileStat, const FormatStat& formStat, const std::string& pathname)
{
	// inode permission_and_mode nlinks userid groupid size lm_date pathname
	printf("%*lu %s %*lu %*u %*u %*lu ", 
												digitCount(formStat.max_inode), fileStat.inode,
												fileStat.getMode().c_str(),
												digitCount(formStat.max_nlink), fileStat.nlink,
												digitCount(formStat.max_uid), fileStat.uid,
												digitCount(formStat.max_gid), fileStat.gid,
												digitCount(formStat.max_size), fileStat.size);


	print_time(fileStat.mtime);

	printf(" %s%s", pathname.c_str(), ((fileStat.mode & S_IFMT) == S_IFDIR) ? "/" : "\0");
}



void FormatStat::update(std::size_t pathname_len, const FileStat& fs)
{
	if(pathname_len > max_pathname_len)
		max_pathname_len = pathname_len;

	if(fs.inode > max_inode)
		max_inode = fs.inode;

	if(fs.nlink > max_nlink)
		max_nlink = fs.nlink;

	if(fs.uid > max_uid)
		max_uid = fs.uid;

	if(fs.gid > max_gid)
		max_gid = fs.gid;

	if(fs.size > max_size)
		max_size = fs.size;
}


std::map<std::string,FileStat> fileStatMap;
FormatStat formatStat;











int main(int argc, const char *argv[])
{

	if(argc < 2)
	{
		fprintf(stderr, "Invalid number of args\r\n");	
	}

	DIR *currentDirectory;
	struct dirent* currentDirectoryEntry;
	int fd;
	struct stat fileStat;
  
	errno = 0;

	if((currentDirectory = opendir(argv[1])) == NULL)
	{
		perror("main()");	
		exit(-1);	
	}



	errno = 0;

	while((currentDirectoryEntry = readdir(currentDirectory)) != NULL)
	{
	
		printf("-------------------------------------------\r\n");
		printf("Inode number %lu\r\n", currentDirectoryEntry->d_ino);
		printf("d_off: %lu\r\n", currentDirectoryEntry->d_off);
		printf("Size of returned record: %d\r\n", currentDirectoryEntry->d_reclen);
		printf("File type: %d\r\n", currentDirectoryEntry->d_type);
		
		switch(currentDirectoryEntry->d_type)
		{
			case DT_BLK:
				printf("BLOCK DEVICE\r\n");
				break;

			case DT_CHR:
				printf("CHARACTER DEVICE\r\n");
				break;

			case DT_DIR:
				printf("DIRECTORY\r\n");
				break;

			case DT_FIFO:
				printf("NAMED FIFO\r\n");
				break;

			case DT_LNK:
				printf("SYMBOLIC LINK\r\n");
				break;

			case DT_REG:
				printf("REGULAR FILE\r\n");
				break;

			case DT_SOCK:
				printf("UNIX DOMAIN SOCKET\r\n");
				break;

			case DT_UNKNOWN:
				printf("UNKOWN FILE TYPE\r\n");
				break;
		}

		printf("Filename: %s\r\n", currentDirectoryEntry->d_name);


		if((fd = open(currentDirectoryEntry->d_name, O_RDONLY)) == -1)
		{
			perror("main()");
			exit(-1);
		}

		if(fstat(fd, &fileStat) == -1)
		{
			perror("main()");
			exit(-1);
		}


		printf("st_dev: %lu\r\n", fileStat.st_dev);
		printf("st_ino: %lu\r\n", fileStat.st_ino);
		printf("st_mode: %u\r\n", fileStat.st_mode);
		printf("st_nlink: %lu\r\n", fileStat.st_nlink);
		printf("st_uid: %u\r\n", fileStat.st_uid);
		printf("st_gid: %u\r\n", fileStat.st_gid);
		printf("st_rdev: %lu\r\n", fileStat.st_rdev);
		printf("st_size: %lu\r\n", fileStat.st_size);
		printf("st_blksize: %lu\r\n", fileStat.st_blksize);
		printf("st_blocks: %lu\r\n", fileStat.st_blocks);
		printf("st_atim: %lu\r\n", fileStat.st_atime);

		printf("-------------------------------------------------------\r\n\r\n");


		FileStat fs(fileStat.st_ino,
							  fileStat.st_mode,
								fileStat.st_nlink,
								fileStat.st_uid,
								fileStat.st_gid,
								fileStat.st_size,
								fileStat.st_mtime);

		formatStat.update(strlen(currentDirectoryEntry->d_name), fs);

		fileStatMap.insert({currentDirectoryEntry->d_name, fs});	

	}

	if(errno != 0)
	{
		perror("main()");
		exit(-1);
	}

	if(closedir(currentDirectory) == -1)
	{
		perror("main()");
		exit(-1);
	}

	std::cout << "MAPA" << std::endl;
	for(auto it = std::begin(fileStatMap); it != std::end(fileStatMap); ++it)
	{
		print(it->second, formatStat, it->first);
		printf("\r\n");
	}

	int terminalFd, passwdFd, groupFd;

	if((terminalFd = dup(STDIN_FILENO)) == -1)
	{
		perror("main()");
		exit(-1);
	}

	if((passwdFd = open("/etc/passwd", O_RDONLY)) == -1)
	{
		perror("main()");
		exit(-1);
	}

	if((groupFd = open("/etc/group", O_RDONLY)) == -1)
	{
		perror("main()");
		exit(-1);
	}

	if(dup2(passwdFd, STDIN_FILENO) == -1)
	{
		perror("main()");
		exit(-1);
	}

	passwd_parse();

	if(dup2(groupFd, STDIN_FILENO) == -1)
	{
		perror("main()");
		exit(-1);
	}


	group_parse();


	if(dup2(terminalFd, STDIN_FILENO) == -1)
	{
		perror("main()");
		exit(-1);
	}

	std::cout << "ENDED PARSING" << std::endl;
	

	return 0;
}
