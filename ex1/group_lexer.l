%option prefix="group_"
%option noyywrap

%{
#include <string>
#include "group_parser.tab.h"

%}


%%

[^:\n]+				{ group_lval.strPtr = new std::string(yytext); return GROUP_TOKEN; }
[:]						{ return GROUP_DELIM; }
[\n]					{ return GROUP_NEWLINE; }


%%

