%define parse.error verbose
%define api.prefix {passwd_}

%{
#include<iostream>
#include<string>
#include<map>

std::map<uid_t,std::string> uidToUsernameMap;

int yylex();
void yyerror(const char *error);


%}

%token<strPtr> PASSWD_TOKEN 
%token PASSWD_DELIM PASSWD_NEWLINE

%union{
	std::string *strPtr;
};

%type<strPtr> item

%%


file_lines:			file_line PASSWD_NEWLINE
							|	file_line PASSWD_NEWLINE file_lines
;

file_line: 			item PASSWD_DELIM 
								item PASSWD_DELIM
								item PASSWD_DELIM
								item PASSWD_DELIM
								item PASSWD_DELIM
								item PASSWD_DELIM
								item 					{
																if($1 == nullptr || $5 == nullptr)
																{
																	std::cerr << "Invalid file format" << std::endl;
																	exit(-1);
																}
													
																uidToUsernameMap.insert({std::stoul(*$5), *$1});
																delete $1;
																delete $3;
																delete $5;
																delete $7;
																delete $9;
																delete $11;
																delete $13;
															}
;

item:						%empty				{ $$ = nullptr; } 
						 |  PASSWD_TOKEN	{ $$ = $1; }
;


%%

void yyerror(const char *error){
	std::cerr << "yyerror(): " << error << std::endl;
}

/*
int main()
{
	passwd_parse();
	
	for(auto it = std::begin(uidToUsernameMap); it != std::end(uidToUsernameMap); ++it)
		std::cout << it->first << " " << it->second << std::endl;
}
*/
