#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

const char moveUP[] = "\033[A";
const char moveDOWN[] = "\033[B";
const char moveFORWARD[] = "\033[C";
const char moveBACKWARD[] = "\033[D";
const char eraseScreenCmd[] = "\033[2J";
const char startCursorPosCmd[] = "\033[1;1f";
const char insertModeCmd[] = "\033[4h";
const char replaceModeCmd[] = "\033[4l";
const char deleteCharacterCmd[] = "\010\033[1P";

#define INSERT_MODE 0
#define COMMAND_MODE 1
#define REPLACE_MODE 2

int process_command_mode();
int process_insert_mode();
int process_replace_mode();

void command_mode_status_line();
void insert_mode_status_line();
void replace_mode_status_line();

void setup_insert_mode();
void setup_replace_mode();
void setup_command_mode();

int get_cursor_pos(int *row, int *col);
int get_terminal_dim(int *row, int *col);

void fatal(char *message);
void fatal2(char *message);

struct termios orig_termios;
int ttyfd = STDIN_FILENO;

int is_terminal_resized = 1;
int t_row_sz, t_col_sz, t_row, t_col;

void (*status_line[3])() = { insert_mode_status_line,
														 command_mode_status_line,
														 replace_mode_status_line
													 };


ssize_t safe_read(int fd, void *buff, size_t count)
{
	ssize_t ret_val;

	while((ret_val = read(fd, buff, count)) == -1 && errno == EINTR);

	return ret_val;
}



ssize_t safe_write(int fd, const void *buff, size_t count)
{
	ssize_t ret_val;

	while((ret_val = write(fd, buff, count)) == -1 && errno == EINTR);

	return ret_val;
}



void terminal_resize_handler(int)
{
	is_terminal_resized = 1;
}


void setup_resize_handler()
{
	struct sigaction sa;

	sa.sa_handler = terminal_resize_handler;
	sigemptyset(&sa.sa_mask);	
	sa.sa_flags = 0;

	sigaction(SIGWINCH, &sa, NULL);
}






void command_mode_status_line()
{
	char buff1[64];
	char buff2[64];

	int buff1_sz, buff2_sz;


	if((buff1_sz = sprintf(buff1, "%d:%d", t_row, t_col)) < 0)
		fatal2("sprintf(): fail");

	if((buff2_sz = sprintf(buff2, "\033[%d;%df", 1000, t_col_sz - strlen(buff1))) < 0)
		fatal2("sprintf(): fail");



	safe_write(STDOUT_FILENO, "\0337", 2); // save the cursor position and attributes
	safe_write(STDOUT_FILENO, "\033[1000;1f", 9); // move cursor to the bottom of term
	safe_write(STDOUT_FILENO, "\033[2K", 4);	// delete line at bottom
	safe_write(STDOUT_FILENO, "\033[1;44m", 7); // set attributes to terminal output, bg-color and bold 
	safe_write(STDOUT_FILENO, " COMMAND ", 9); // print status

	safe_write(STDOUT_FILENO, buff2, buff2_sz);
	safe_write(STDOUT_FILENO, buff1, buff1_sz);
	
	safe_write(STDOUT_FILENO, "\0338", 2); // restore the cursor position and attributes
}



void insert_mode_status_line()
{
	char buff1[64];
	char buff2[64];

	int buff1_sz, buff2_sz;


	if((buff1_sz = sprintf(buff1, "%d:%d", t_row, t_col)) < 0)
		fatal2("sprintf(): fail");

	if((buff2_sz = sprintf(buff2, "\033[%d;%df", 1000, t_col_sz - strlen(buff1))) < 0)
		fatal2("sprintf(): fail");


	safe_write(STDOUT_FILENO, "\0337", 2);
	safe_write(STDOUT_FILENO, "\033[1000;1f", 9);
	safe_write(STDOUT_FILENO, "\033[2K", 4);	
	safe_write(STDOUT_FILENO, "\033[1;42m", 7); 
	safe_write(STDOUT_FILENO, " INSERT ", 8);

	safe_write(STDOUT_FILENO, buff2, buff2_sz);
	safe_write(STDOUT_FILENO, buff1, buff1_sz);
	
	safe_write(STDOUT_FILENO, "\0338", 2); 
}




void replace_mode_status_line()
{
	char buff1[64];
	char buff2[64];

	int buff1_sz, buff2_sz;


	if((buff1_sz = sprintf(buff1, "%d:%d", t_row, t_col)) < 0)
		fatal2("sprintf(): fail");

	if((buff2_sz = sprintf(buff2, "\033[%d;%df", 1000, t_col_sz - strlen(buff1))) < 0)
		fatal2("sprintf(): fail");


	safe_write(STDOUT_FILENO, "\0337", 2);
	safe_write(STDOUT_FILENO, "\033[1000;1f", 9);
	safe_write(STDOUT_FILENO, "\033[2K", 4);	
	safe_write(STDOUT_FILENO, "\033[1;41m", 7); 
	safe_write(STDOUT_FILENO, " REPLACE ", 9);

	safe_write(STDOUT_FILENO, buff2, buff2_sz);
	safe_write(STDOUT_FILENO, buff1, buff1_sz);
	
	safe_write(STDOUT_FILENO, "\0338", 2); 
}







int tty_reset()
{
	if(tcsetattr(ttyfd, TCSAFLUSH, &orig_termios) < 0)
		return -1;
	return 0;
}

void tty_atexit()
{
	tty_reset();
}

void fatal(char *message)
{
	perror(message);
	exit(-1);
}

void fatal2(char *message)
{
	fprintf(stderr, "%s", message);
	exit(-1);
}


void set_tty()
{
	struct termios raw;
	raw = orig_termios;

	raw.c_lflag &= ~(ECHO | ICANON | ISIG);
	raw.c_cc[VMIN] = 1;
	raw.c_cc[VTIME] = 1;
	//raw.c_cc[VTIME] = 1;
	if(tcsetattr(ttyfd, TCSAFLUSH, &raw) < 0)
		fatal("Can not set raw mode");
}

void update_terminal_pos()
{
	int row, col;

	if(is_terminal_resized != 0 && get_terminal_dim(&row, &col) == 0)
	{
		t_row_sz = row;
		t_col_sz = col;
		is_terminal_resized = 0;
	}

	if(get_cursor_pos(&row, &col) == 0)
	{
		t_row = row;
		t_col = col;
	}
}


int terminal()
{
	int state = COMMAND_MODE;
	setup_command_mode();
	setup_resize_handler();
	
	// need upgrade
	safe_write(STDOUT_FILENO, eraseScreenCmd, 4);	
	safe_write(STDOUT_FILENO, startCursorPosCmd, 6);
	update_terminal_pos();


	/*
	write(STDOUT_FILENO, "\033[65;81p", 8); //remap esc to \034
	char buff[64];
	char buff2[64];
	int n = 0;
	n = safe_read(ttyfd, buff, 10);
	for(int i = 0; i < n; ++i)
	{
		sprintf(buff2, "%d ", buff[i]);
		write(STDOUT_FILENO, buff2, strlen(buff2));
	}
	*/
	//write(STDOUT_FILENO, "OOOK", 4);
	//exit(0);
	//write(STDOUT_FILENO, "\033[27;\"\034\"p", 8); //remap esc to \034
	
	while(1)
	{
		switch(state)
		{
			case COMMAND_MODE:
				state = process_command_mode();
				break;

			case INSERT_MODE:
				state = process_insert_mode();
				break;

			case REPLACE_MODE:
				state = process_replace_mode();
		}

		update_terminal_pos();
	}

}



int process_command_mode()
{
	int n;
	char c;
	
	status_line[COMMAND_MODE]();

	if((n = safe_read(ttyfd, &c, 1)) < 0)
		fatal("Error in reading");


	if(n > 0)
		switch(c)
		{
			case 'H':
			case 'h':
				safe_write(STDOUT_FILENO, moveBACKWARD, 3);	
				break;

			case 'J':
			case 'j':	
				safe_write(STDOUT_FILENO, moveDOWN, 3);	
				break;

			case 'K':
			case 'k':
				safe_write(STDOUT_FILENO, moveUP, 3);	
				break;

			case 'L':
			case 'l':
				safe_write(STDOUT_FILENO, moveFORWARD, 3);	
				break;

			case 'I':
			case 'i':
				setup_insert_mode();	
				return INSERT_MODE;


			case 'R':
			case 'r':
				setup_replace_mode();
				return REPLACE_MODE;


			case 'Q':
			case 'q':
				exit(0);

		}

	return COMMAND_MODE;
}


int get_cursor_pos(int *row, int *col)
{
	char buff[32];
	int n;
	char *startPos = &buff[2]; 
	char *endPos;

	if((n = safe_write(STDOUT_FILENO, "\033[6n", 4)) == -1)
		fatal("Error in writting 'read cursor position' command");
	
	if(n != 4)
		fatal2("'read cursor position' command is not written in entirety");

	if((n = safe_read(ttyfd, buff, 20)) == -1)
		fatal("Error in reading cursor position");
	
	if(n < 5)
	{
		//fatal2("Invalid response to cursor position read command");
		return -1;	
	}	
	//buff[n] = '\0';

	if(buff[0] != '\033' && buff[1] != '[')
		return -2;	
		//fatal2("Invalid reponse format to cursor position read command");

	*row = strtol(startPos, &endPos, 10);

	if(*row == 0 && startPos == endPos)
		return -3;
		//fatal2("Invalid response format to cursor position read command, expected number1");

	if(*endPos != ';')
		return -4;
		//fatal2("Invalid response format to cursor position read command, expected ';'");

	startPos = endPos + 1;

	*col = strtol(startPos, &endPos, 10);

	if(*col == 0 && startPos == endPos)
		return -5;	
		//fatal2("Invalid response format to cursor position read command, expected number2");

	if(*endPos != 'R' && buff + n == endPos + 1)
		return -6;	
		//fatal2("Invalid response format to cursor position read command, expected 'R'");

	return 0;
}


int get_terminal_dim(int *row, int *col)
{
	int status;

	safe_write(STDOUT_FILENO, "\0337", 2);
	safe_write(STDOUT_FILENO, "\033[1000;1000f", 12);
	status =  get_cursor_pos(row, col);
	safe_write(STDOUT_FILENO, "\0338", 2);

	return status;
}


int process_insert_mode()
{	
	int n;
	char c;
	char is_escape_seq = 0;

	status_line[INSERT_MODE]();
	
	if((n = safe_read(ttyfd, &c, 1)) < 0)
		fatal("Error in reading");

	if(n > 0)
		switch(c)
		{
			case '\033':
			{
				// this code distinguish between ESC and escape sequence
				fd_set read_set;
				struct timeval timeout;
				int rv;

				FD_ZERO(&read_set);
				FD_SET(ttyfd, &read_set);
				timeout.tv_sec = 0;
				timeout.tv_usec = 0;

				if((rv = select(1, &read_set, NULL, NULL, &timeout)) == -1)
					perror("Error select()");

				if(rv == 0)
				{	
					setup_command_mode();	
					return COMMAND_MODE;
				}
				else
					is_escape_seq = 1; // change state machine to esc seq mode	
			}
			break;

			case 127:
				safe_write(STDOUT_FILENO, deleteCharacterCmd, 5);
				break;

			default:
				if(is_escape_seq == 0)
					safe_write(STDOUT_FILENO, &c, 1);
				else // in the case od escape sequence we need to treat them on specific way
				{
					if(c == 'R') // at the end of the only one escape sequence which can occur at input is 'R', so when we reach 'R' the escape sequence is compleated  
						is_escape_seq = 0;
				}	
		}

	return INSERT_MODE;
}




int process_replace_mode()
{
	
	int n;
	char c;
	char is_escape_seq = 0;
	
	status_line[REPLACE_MODE]();

	if((n = safe_read(ttyfd, &c, 1)) < 0)
		fatal("Error in reading");

	if(n > 0)
		switch(c)
		{
			case '\033':
			{
				fd_set read_set;
				struct timeval timeout;
				int rv;

				FD_ZERO(&read_set);
				FD_SET(ttyfd, &read_set);
				timeout.tv_sec = 0;
				timeout.tv_usec = 0;

				if((rv = select(1, &read_set, NULL, NULL, &timeout)) == -1)
					perror("Error select()");

				if(rv == 0)
				{	
					setup_command_mode();	
					return COMMAND_MODE;
				}
				else
					is_escape_seq = 1;	
			}
			break;

			case 127:
				safe_write(STDOUT_FILENO, "\010", 1);
				break;
			
			default:
				if(is_escape_seq == 0)
					safe_write(STDOUT_FILENO, &c, 1);
				else
				{
					if(c == 'R')
						is_escape_seq = 0;
				}
			}

	return REPLACE_MODE;
}




void setup_insert_mode()
{
	safe_write(STDOUT_FILENO, insertModeCmd, 4);
}

void setup_replace_mode()
{
	safe_write(STDOUT_FILENO, replaceModeCmd, 4);
}

void setup_command_mode()
{
	safe_write(STDOUT_FILENO, replaceModeCmd, 4);
}







int process_term(){
	int n;
	char c_in, c_out;
	
	while(1)
	{
		n = safe_read(ttyfd, &c_in, 1);
		if(n < 0)
			fatal("read error");
		
		if(n > 0)
		{
			switch(c_in)
			{
				case 'q':
					return 0;

				default:
					write(STDOUT_FILENO, &c_in, 1);
			}
		}
	}

}


int main(void)
{
	if(!isatty(ttyfd))
		fatal("STDIN is not connected to terminal");

	if(tcgetattr(ttyfd, &orig_termios) < 0)
		fatal("Can not get tty settings");

	if(atexit(tty_atexit) != 0)
		fatal("atexit(): can not register tty reset function");

	set_tty();
	terminal();
	process_term();

	return 0;
}
